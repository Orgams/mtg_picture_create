#!/usr/bin/env python3
# coding=utf8

#PYTHON_ARGCOMPLETE_OK

"""Create picture with mtg card picture from scryfall.com, with requests in 3 steps :
   Download art picture from all carts in requests,
   Edit picture : insert name, artist and flavor (deactivate in 'all'),
   Compose picture : agreate all cards on request in one big picture,
"""

import json
import requests
import shutil
import textwrap
import math
import time
import argparse, argcomplete
import sys
import logging
import logging.handlers
import os

from pathlib import Path

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

logger = logging.getLogger(os.path.splitext(os.path.basename(sys.argv[0]))[0])

api_url_base = 'https://api.scryfall.com'

headers = {'Content-Type': 'application/json'}

home = str(Path.home())

rep_img = home + '/Images/magic'

ext_img = '.png'

lang = '+lang%3Afr'

screen_ratio=1920/1080

img_w_crop, img_h_crop = (616, 452)
img_w_card, img_h_card = (745, 1040)

options = None

#utils

def get_json(url):
    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        return json.loads(response.content.decode('utf-8'))
    else:
        logger.error("La réponse du WS n'est pas celle attendu : code "+str(response.status_code))
        exit(1)

def save_img(url, file):
    #get stream
    resp = requests.get(url, stream=True)

    # Open a local file with wb ( write binary ) permission.
    local_file = open(file, 'wb')

    # Set decode_content value to True, otherwise the downloaded image file's size will be zero.
    resp.raw.decode_content = True

    # Copy the response stream raw data to local image file.
    shutil.copyfileobj(resp.raw, local_file)

    # Remove the image url response object.
    del resp

#request

def get_info_cards_lang(req, lang):

    # effectuer la requete en indiquant la langue
    api_url = '{0}/cards/search?q={1}{2}'.format(api_url_base, req, lang)
    res = get_json(api_url)

    # effectuer la requete sans la langue si la requete échoue
    if res == None:
        api_url = '{0}/cards/search?q={1}'.format(api_url_base, req)
        res = get_json(api_url)

    return res

def get_info_cards(req):
    api_url = '{0}/cards/search?q={1}'.format(api_url_base, req)
    logger.debug("Call : " + api_url)
    json = get_json(api_url)

    # si le mode auto est activé et qu'il y a moins de 8 cartes à traiter activer le mode évantail
    if options.automatique_type and json is not None :
        if len(json['data']) < 8:
            options.eventail=True
            logger.debug("mode auto : mode évantail activé")
        else:
            options.eventail=False
            logger.debug("mode auto : mode évantail déactivé")

    return json

# get name

def get_printed_name(card):
    if "printed_name" in card:
        return card['printed_name']
    else:
        return card['name']

def get_card_name(req, card):
    card_name = card['name']

    if options.eventail:
        card_name = card_name + '_eventail.png'
    else:
        card_name = card_name + '.jpg'

    card_name=("/"+req + "_" + card_name).replace('/', '').replace(' ', '_').replace('\n','')
    logger.debug("card_name : " + card_name)
    return card_name

#Get path

def get_base_card_path(req, card):
    return (rep_img + "/" + get_card_name(req, card))

def get_compo_card_dir_path(req):
    return rep_img + '/compo'

def get_compo_card_path(req):
    eventail_str = ""
    if options.eventail:
        eventail_str = "_eventail"
    compo_name = ('compo_' + req + eventail_str + ext_img).replace('/', '').replace('\n','')
    compo_file = (get_compo_card_dir_path(req) + '/'+compo_name).replace(' ', '_')
    logger.info(compo_file)
    return compo_file

# Utilitaire

def edit_img(req, card):
    file_path = get_base_card_path(req, card)
    logger.debug(file_path)
    try:
        # Get editable image
        img = Image.open(file_path)
        draw = ImageDraw.Draw(img)

        # Add image name and artist on image
        draw_with_shadow(draw, (10, 0), get_printed_name(card)+" - "+card['artist'])

        # Add flavor on image if exist
        if "flavor_text" in card:
            draw_with_shadow_bottom(draw, (10, 452), card["flavor_text"])

        # Save image
        img.save(file_path)
    except FileNotFoundError as e:
        logger.exception("%s", e)

def draw_with_shadow_bottom(draw, xy, text_in):
    nb_char = 60
    sep = '\n'
    text = sep.join(text_in[i:i+nb_char] for i in range(0, len(text_in), nb_char))
    nb_eof = text.count(sep)
    x = xy[0]
    y = xy[1]
    y-=20*(nb_eof+1)
    draw_with_shadow(draw, (x, y), text)

def draw_with_shadow(draw, xy, text):
    size_shadow=1
    font = ImageFont.truetype("DejaVuSansMono.ttf", 16)
    rowor = (0,0,0,255)
    x = xy[0]
    y = xy[1]
    draw.multiline_text((x-size_shadow, y), text, rowor, font)
    draw.multiline_text((x+size_shadow, y), text, rowor, font)
    draw.multiline_text((x, y-size_shadow), text, rowor, font)
    draw.multiline_text((x, y+size_shadow), text, rowor, font)

    draw.multiline_text((x-size_shadow, y-size_shadow), text, rowor, font)
    draw.multiline_text((x+size_shadow, y-size_shadow), text, rowor, font)
    draw.multiline_text((x-size_shadow, y+size_shadow), text, rowor, font)
    draw.multiline_text((x+size_shadow, y+size_shadow), text, rowor, font)

    draw.multiline_text((x, y), text,(255,255,255),font)

def get_flavor(req):
    cards = get_info_cards_lang(req, lang)
    if cards is not None:
        for card in cards['data']:
            if "flavor_text" in card:
                logger.info(get_printed_name(card), "\t : ", card["flavor_text"])
    else:
        logger.warning('[!] Request Failed')

def nb_row_col(nb_cards, col=1, row=1):


    for nb_card in range(1, nb_cards+1):
        if nb_card>col*row:
            if (col*img_w_crop)/(row*img_h_crop) < screen_ratio:
                col+=1
            else:
                row+=1

    return (col, row)

# Commands

def dl_cards():

    logger.debug("options.requests : " + str(options.requests))
    for request in options.requests:
        cards = get_info_cards(request)

        if cards is not None:
            for card in cards['data']:
                logger.info('dl '+card['name'])
                file_name = get_base_card_path(request, card)
                logger.debug("file_name : " + file_name)
                if options.eventail:
                    type_img="png";
                else:
                    type_img="art_crop"
                if "image_uris" in card:
                    save_img(card['image_uris'][type_img], file_name)
        else:
            logger.warning('[!] Request Failed')

def keep_dl_cards(request):
    cards_req = get_info_cards(request)

    cards = []

    # Recuperer les cartes effectivement telecharger
    for card in cards_req['data']:
        file_path = get_base_card_path(request, card)
        logger.debug("file_path : " + str(file_path))
        if Path(file_path).is_file():
            cards.append(card)
            logger.info("Carte effectivement telecharger : " + card['name'])
    return cards

def save_img_sortie(request, compo):
    #Enregister l'image de sortie
    Path(get_compo_card_dir_path(request)).mkdir(parents=True, exist_ok=True)
    image_compo = get_compo_card_path(request)
    compo.save(image_compo)
    logger.info("composition d'image creer : " + image_compo)

def compo_cards():
    for request in options.requests:
        cards = keep_dl_cards(request)
        if options.eventail:
            compo_cards_eventail(request, cards)
        else:
            compo_cards_mosaique(request, cards)

def compo_cards_eventail(request, cards):

    #creer l'image de sortie
    compo = Image.new('RGB', (5*img_w_card, 2*img_h_card), (0,0,0))

    #Ajouter les images de cartes
    compo_w, compo_h = compo.size
    nb_point = len(cards)+2
    ts = [t/(nb_point-1) for t in range(nb_point)]
    xys = [(0, int(compo_h*0.75)), (compo_w//2, 0), (compo_w, int(compo_h*0.75))]
    bezier = make_bezier(xys)
    points = bezier(ts)
    points = points[1:len(points)-1]

    angles_max = 35
    pas = (angles_max*2-1)//(len(cards)-1)
    angles = reversed(range(-angles_max, angles_max, pas))
    for card, angle, point in zip(cards, angles, points):
        file_path = get_base_card_path(request, card)

        try:

            #calcule géometrique
            img = Image.open(file_path, 'r')
            img = img.rotate(angle, expand=True)
            img_w, img_h = img.size
            center_card = -img_w//2, -img_h//2
            position_card = img_w_card//2, img_h_card//2
            compo.paste(img, (center_card[0]+int(point[0]), center_card[1]+int(point[1])), img)

        except FileNotFoundError:
            None
    save_img_sortie(request, compo)



def compo_cards_mosaique(request, cards):

        nb_cards = len(cards)

        nb_cards_restante = nb_cards

        nb_col, nb_row = nb_row_col(nb_cards_restante)
        logger.debug("pour "+str(nb_cards_restante)+" cartes nous avons " + str(nb_col) + " colonnes et "+ str(nb_row) + " lignes")

        nb_cards_restante_last_row = nb_cards_restante%nb_col
        void_last_row = nb_col - nb_cards_restante_last_row

        if(options.eventail):
            img_w, img_h = img_w_card, img_h_card
        else:
            img_w, img_h = img_w_crop, img_h_crop

        #creer l'image de sortie
        compo = Image.new('RGBA', (nb_col*img_w, nb_row*img_h), (0,0,0,0))

        logger.info('compo '+request)

        #Ajouter les images de cartes
        index = 0
        for row in range(0, nb_row):
            # Mettre une carte par colone si la nombre de carte qu'il reste est multiple du nb de colone sinon en mettre une de moins
            if nb_cards_restante%nb_col == 0:
                nb_carte_en_moins = nb_col
                offset_weight=0
            else:
                nb_carte_en_moins = nb_col-1
                offset_weight=int(img_w/2)
            nb_cards_restante -= nb_carte_en_moins

            for col in range(0, nb_carte_en_moins):
                if index < nb_cards:
                    card = cards[index]
                    logger.info ('add '+card['name'])
                    file_path = get_base_card_path(request, card)
                    try:
                        img = Image.open(file_path, 'r')
                        offset = (col*img_w + offset_weight, row*img_h)
                        compo.paste(img, offset)
                    except FileNotFoundError:
                        None
                index += 1

        save_img_sortie(request, compo)

def make_parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(description=sys.modules[__name__].__doc__)

    parser.add_argument("actions", nargs='+',
                        choices=['dl', 'compo'],
                        default=['dl', 'compo'],
                        help="action to be taken on the result of the request (default all)")

    parser.add_argument("--dryrun", "-y", action="store_true",
                   default=False,
                   help="passage à blanc")

    type_compo = parser.add_mutually_exclusive_group()
    type_compo.add_argument("--automatique_type", "-a", action="store_true",
                   default=False,
                   help="Choisir automatiquement entre le mode éventail et le mode mosaïque")
    type_compo.add_argument("--eventail", "-e", action="store_true",
                   default=False,
                   help="Générer une image en évantail plustôt qu'en mosaïque")

    requests = parser.add_mutually_exclusive_group(required=True)
    requests.add_argument("--requests", '-r', nargs='+',
                        help="Requetes à lancer sur scryfall.com API")
    requests.add_argument("--file", '-f',
                        help="fichier avec toutes les requetes à lancer sur scryfall.com API")

    verbose = parser.add_mutually_exclusive_group()
    verbose.add_argument("--debug", "-d", action="store_true",
                   default=False,
                   help="enable debugging")
    verbose.add_argument("--silent", "-s", action="store_true",
                   default=False,
                   help="don't log to console")

    return parser

def setup_logging():
    """Configure logging."""
    root = logging.getLogger("")
    root.setLevel(logging.WARNING)
    logger.setLevel(options.debug and logging.DEBUG or logging.INFO)
    if not options.silent:
        if not sys.stderr.isatty():
            facility = logging.handlers.SysLogHandler.LOG_DAEMON
            sh = logging.handlers.SysLogHandler(address='/dev/log', facility=facility)
            sh.setFormatter(logging.Formatter("{0}[{1}]: %(message)s".format(logger.name, os.getpid())))
            root.addHandler(sh)
        else:
            ch = logging.StreamHandler()
            ch.setFormatter(logging.Formatter("%(levelname)s[%(name)s] %(message)s"))
            root.addHandler(ch)

def make_bezier(xys):
    # xys should be a sequence of 2-tuples (Bezier control points)
    n = len(xys)
    combinations = pascal_row(n-1)
    def bezier(ts):
        # This uses the generalized formula for bezier curves
        # http://en.wikipedia.org/wiki/B%C3%A9zier_curve#Generalization
        result = []
        for t in ts:
            tpowers = (t**i for i in range(n))
            upowers = reversed([(1-t)**i for i in range(n)])
            coefs = [c*a*b for c, a, b in zip(combinations, tpowers, upowers)]
            result.append(
                tuple(sum([coef*p for coef, p in zip(coefs, ps)]) for ps in zip(*xys)))
        return result
    return bezier

def pascal_row(n):
    # This returns the nth row of Pascal's Triangle
    result = [1]
    x, numerator = 1, n
    for denominator in range(1, n//2+1):
        # print(numerator,denominator,x)
        x *= numerator
        x /= denominator
        result.append(x)
        numerator -= 1
    if n&1 == 0:
        # n is even
        result.extend(reversed(result[:-1]))
    else:
        result.extend(reversed(result))
    return result

if __name__ == "__main__":
    # Create parser for argumants
    parser = make_parse_args()

    # execute autocompete
    argcomplete.autocomplete(parser)

    # Parse arguments
    options = parser.parse_args(sys.argv[1:])

    # Setup logging with option
    setup_logging()

    # Initialiser les requetes avec le fichier si passer en parametre
    if options.file is not None:
        options.requests = []
        f = open(options.file, "r")
        for l in f:
            options.requests.append(l)

    # Process all requestes
    try:
        if 'dl' in options.actions:
            logger.debug("Use dl_cards fontion")
            if not options.dryrun:
                dl_cards()

        if 'compo' in options.actions:
                logger.debug("Use compo_cards")
                if not options.dryrun:
                    compo_cards()

    except Exception as e:
        logger.exception("%s", e)
        sys.exit(1)

    sys.exit(0)